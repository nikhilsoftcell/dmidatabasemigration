package in.mahesh.dmidatabasemigration;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by mahesh on 14/3/18.
 */
@Dao
public interface DMIDao {

    @Insert
    void insert(ImagePendingDBStore imagePendingDBStore);

    @Query("UPDATE ImagePendingDBStore SET imageTitle = :ImageTitle WHERE documentName = :uploadFileType AND applicantPosition = :applicantPos ")
    void updateTitleForImage(String uploadFileType, String ImageTitle, int applicantPos);


    @Query("DELETE FROM ImagePendingDBStore WHERE applicantPosition = :position")
    void deleteImageForCoApplicant(int position);

    @Query("SELECT * FROM ImagePendingDBStore WHERE documentName = :documentName")
    List<ImagePendingDBStore> getBusinessPhotoDocId(String documentName);

    @Query("SELECT * FROM ImagePendingDBStore WHERE documentName = :docName AND applicantPosition = :applicantPos LIMIT 1")
    ImagePendingDBStore getOtherPhotoDocId(String docName, int applicantPos);

    @Query("SELECT * FROM ImagePendingDBStore WHERE mobileNumber = :mobileNo AND synced = 0")
    List<ImagePendingDBStore> getImagesToUpload(String mobileNo);

    @Delete
    void getSyncedImagesToDelete(List<ImagePendingDBStore> imagePendingDBStoreList);

    @Query("UPDATE ImagePendingDBStore SET synced = 1 WHERE imageIdRandom = :imageId")
    void updateImageSynced(final String imageId);

    @Query("SELECT * FROM ImagePendingDBStore WHERE mobileNumber = :uniqueID AND documentName = :documentName AND applicantPosition = :position")
    ImagePendingDBStore getPathForImageType(String uniqueID, String documentName, int position);


    @Query("SELECT * FROM ImagePendingDBStore WHERE imageType LIKE :uploadImageType AND referenceId = :gonogoID")
    List<ImagePendingDBStore> getImagesWithReferenceAndType(String gonogoID,String uploadImageType);

}
