package in.mahesh.dmidatabasemigration;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by mahesh on 14/3/18.
 */
@Entity(tableName = "ImagePendingDBStore", indices = {@Index(value = "referenceId")})
public class ImagePendingDBStore {

    @PrimaryKey()
    @NonNull
    private String imageIdRandom;
    private String referenceId;
    private String mobileNumber;
    private String imageTitle;
    private String imageType;
    private String imageReason;
    private Date createdAt;
    private Date updatedAt;
    private String imageAbsolutePath;
    private boolean synced;
    private String customerFirstName;
    private String customerMiddleName;
    private String customerLastName;
    private String dasId;
    private String documentName;
    private int imageResolution;
    private int applicantPosition;

    public String getImageIdRandom() {
        return imageIdRandom;
    }

    public void setImageIdRandom(String imageIdRandom) {
        this.imageIdRandom = imageIdRandom;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageReason() {
        return imageReason;
    }

    public void setImageReason(String imageReason) {
        this.imageReason = imageReason;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getImageAbsolutePath() {
        return imageAbsolutePath;
    }

    public void setImageAbsolutePath(String imageAbsolutePath) {
        this.imageAbsolutePath = imageAbsolutePath;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerMiddleName() {
        return customerMiddleName;
    }

    public void setCustomerMiddleName(String customerMiddleName) {
        this.customerMiddleName = customerMiddleName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getDasId() {
        return dasId;
    }

    public void setDasId(String dasId) {
        this.dasId = dasId;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public int getImageResolution() {
        return imageResolution;
    }

    public void setImageResolution(int imageResolution) {
        this.imageResolution = imageResolution;
    }

    public int getApplicantPosition() {
        return applicantPosition;
    }

    public void setApplicantPosition(int applicantPosition) {
        this.applicantPosition = applicantPosition;
    }

    @Override
    public String toString() {
        return "ImagePendingDdStore{" +
                "imageIdRandom=" + imageIdRandom +
                ", referenceId='" + referenceId + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", imageTitle='" + imageTitle + '\'' +
                ", imageType='" + imageType + '\'' +
                ", imageReason='" + imageReason + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", imageAbsolutePath='" + imageAbsolutePath + '\'' +
                ", synced=" + synced +
                ", customerFirstName='" + customerFirstName + '\'' +
                ", customerMiddleName='" + customerMiddleName + '\'' +
                ", customerLastName='" + customerLastName + '\'' +
                ", dasId='" + dasId + '\'' +
                ", documentName='" + documentName + '\'' +
                ", imageResolution=" + imageResolution +
                '}';
    }
}
