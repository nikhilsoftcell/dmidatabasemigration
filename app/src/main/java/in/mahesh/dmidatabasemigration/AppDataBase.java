package in.mahesh.dmidatabasemigration;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

/**
 * Created by mahesh on 14/3/18.
 */
@Database(entities = {ImagePendingDBStore.class},version = 1 ,exportSchema = false)
@TypeConverters(DateTypeConverter.class)
public abstract class AppDataBase extends RoomDatabase {

    public static final String DATABASE_NAME = "DMI.db";

    public abstract DMIDao dmiDao();



}
