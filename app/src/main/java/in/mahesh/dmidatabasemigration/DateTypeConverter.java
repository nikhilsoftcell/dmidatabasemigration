package in.mahesh.dmidatabasemigration;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by mahesh on 14/3/18.
 */

public class DateTypeConverter {

    @TypeConverter
    public Date fromTimeStamp(Long value){

        return value == null ? null : new Date(value);

    }

    @TypeConverter
    public Long dateToTimeStamp(Date date){
        return date == null ? null : date.getTime();

    }
}
