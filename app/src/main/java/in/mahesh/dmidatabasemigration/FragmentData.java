package in.mahesh.dmidatabasemigration;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mahesh on 15/3/18.
 */

public class FragmentData extends Fragment {


    private String LOG_TAG = FragmentData.class.getSimpleName();


    @BindView(R.id.et_imageID)
    EditText et_imageID;
    @BindView(R.id.et_refID)
    EditText et_refID;
    @BindView(R.id.et_mobile_number)
    EditText et_mobile_number;
    @BindView(R.id.et_image_path)
    EditText et_image_path;
    @BindView(R.id.et_first_name)
    EditText et_first_name;
    @BindView(R.id.et_middle_name)
    EditText et_middle_name;
    @BindView(R.id.et_last_name)
    EditText et_last_name;
    @BindView(R.id.et_dsa_id)
    EditText et_dsa_id;
    @BindView(R.id.et_image_reason)
    EditText et_image_reason;
    @BindView(R.id.et_image_title)
    EditText et_image_title;
    @BindView(R.id.et_image_type)
    EditText et_image_type;
    @BindView(R.id.et_image_resolution)
    EditText et_image_resolution;
    @BindView(R.id.et_document_name)
    EditText et_document_name;
    @BindView(R.id.et_applicant_position)
    EditText et_applicant_position;
    @BindView(R.id.btn_insert)
    Button btn_insert;
    @BindView(R.id.btn_get_data)
    Button btn_get;
    @BindView(R.id.btn_update_data)
    Button btn_update;
    @BindView(R.id.btn_delete)
    Button btn_delete;
    @BindView(R.id.btn_clear)
    Button btnClear;


    private AppDataBase appDataBase;
    private List<ImagePendingDBStore> imagePendingDBStoreList;

    public static FragmentData newInstance() {
        FragmentData fragmentData = new FragmentData();
        return fragmentData;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_data, null, false);
        ButterKnife.bind(this, view);
        appDataBase = Room.databaseBuilder(getActivity(), AppDataBase.class, AppDataBase.DATABASE_NAME).build();
        imagePendingDBStoreList = new ArrayList<>();
        return view;
    }

    @OnClick({R.id.btn_insert, R.id.btn_get_data, R.id.btn_update_data, R.id.btn_delete,R.id.btn_clear})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.btn_insert:
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        ImagePendingDBStore imagePendingDBStore = new ImagePendingDBStore();

                        imagePendingDBStore.setImageIdRandom(et_imageID.getText().toString().trim());
                        imagePendingDBStore.setReferenceId(et_refID.getText().toString().trim());
                        imagePendingDBStore.setMobileNumber(et_mobile_number.getText().toString().trim());
                        imagePendingDBStore.setImageTitle(et_image_title.getText().toString().trim());
                        imagePendingDBStore.setImageType(et_image_type.getText().toString().trim());
                        imagePendingDBStore.setImageReason(et_image_reason.getText().toString().trim());
                        imagePendingDBStore.setImageAbsolutePath(et_image_path.getText().toString().trim());
                        imagePendingDBStore.setCustomerFirstName(et_first_name.getText().toString().trim());
                        imagePendingDBStore.setCustomerMiddleName(et_middle_name.getText().toString().trim());
                        imagePendingDBStore.setCustomerLastName(et_last_name.getText().toString().trim());
                        imagePendingDBStore.setDasId(et_dsa_id.getText().toString().trim());
                        imagePendingDBStore.setDocumentName(et_document_name.getText().toString().trim());
                        imagePendingDBStore.setImageResolution(Integer.parseInt(et_image_resolution.getText().toString().trim()));
                        imagePendingDBStore.setApplicantPosition(Integer.parseInt(et_applicant_position.getText().toString().trim()));
                        imagePendingDBStore.setCreatedAt(new Date());
                        imagePendingDBStore.setUpdatedAt(new Date());

                        appDataBase.dmiDao().insert(imagePendingDBStore);
                        Log.d(LOG_TAG, imagePendingDBStore.toString());


                    }
                }).start();
                break;


            case R.id.btn_get_data:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        imagePendingDBStoreList = appDataBase.dmiDao().getImagesWithReferenceAndType(et_refID.getText().toString().trim(), et_image_type.getText().toString().trim() + "%");
                        for (int i = 0; i < imagePendingDBStoreList.size(); i++) {
                            Log.d("ImagePendingStoreList", imagePendingDBStoreList.get(i).toString());
                        }

                    }
                }).start();
                break;


            case R.id.btn_update_data:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        appDataBase.dmiDao().updateTitleForImage(et_document_name.getText().toString().trim(), et_image_title.getText().toString().trim(), Integer.parseInt(et_applicant_position.getText().toString().trim()));


                    }
                }).start();
                break;


              /*  new Thread(new Runnable() {
                    @Override
                    public void run() {
                        appDataBase.dmiDao().updateImageSynced(et_imageID.getText().toString().trim());
                    }
                }).start();
                break;*/


            case R.id.btn_delete:
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        appDataBase.dmiDao().deleteImageForCoApplicant(Integer.parseInt(et_applicant_position.getText().toString().trim()));

                    }
                }).start();
                break;


            case R.id.btn_clear:
                clearData();
                break;
        }


    }


    public void clearData() {
        et_imageID.setText(null);
        et_refID.setText(null);
        et_mobile_number.setText(null);
        et_image_title.setText(null);
        et_image_type.setText(null);
        et_image_reason.setText(null);
        et_image_path.setText(null);
        et_first_name.setText(null);
        et_middle_name.setText(null);
        et_last_name.setText(null);
        et_dsa_id.setText(null);
        et_document_name.setText(null);
        et_image_resolution.setText(null);
        et_applicant_position.setText(null);


    }
}
